from django.shortcuts import render
from django.http import Http404, JsonResponse
from HalalWeb.models import Restaurant


# Create your views here.
def vote_question(request, restaurant_id):
    try:
        restaurant = Restaurant.objects.get(pk=restaurant_id)
        restaurant.vote += 1
        restaurant.save()
    except Restaurant.DoesNotExist:
        raise Http404("Restaurant does not exit")

    result = {
        'id': restaurant_id,
        'rating': restaurant.rating
    }

    return JsonResponse(result)