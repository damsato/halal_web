from django.urls import path, re_path

from . import views

urlpatterns = [
    path('top', views.top, name='top'),
    path('restaurant', views.restaurant, name='restaurant'),
    #path('create', views.create, name='create'),
    path('list', views.list, name='list'),
    re_path(r'^restaurant/(?P<restaurant_id>[0-9]+)/$',
            views.restaurant, name='restaurant'),
    path("register/", views.register, name='register'),
    path("about/", views.about, name='about'),
]
