from django.shortcuts import render, redirect
from django.utils import timezone
from .forms import RegistrationForm, FindForm, RegisterForm
from .models import Restaurant, Comment
from django.http import Http404, HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate
from django.db.models import Avg
from django.contrib.auth.decorators import login_required


# Create your views here.


def create(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()

    context = {'form': RegistrationForm()}
    return render(request, 'HalalWeb/create.html', context)
    # restaurant = Restaurant(name=request.POST.get('name'), genre=request.POST.get('genre'), menu=request.POST.get('menu'), halal_cer=request.POST.get('halal_cer', '') == 'on', halal_meat=request.POST.get('halal_meat', '') == 'on', p_free=request.POST.get('p_free', '') == 'on', halal_sea=request.POST.get('halal_sea', '') == 'on', halal_meal=request.POST.get('halal_meal', '') == 'on',

    #                       vege_meal=request.POST.get('vege_meal', '') == 'on', muslim_o=request.POST.get('muslim_o', '') == 'on', muslim_c=request.POST.get('muslim_c', '') == 'on', halal_sto=request.POST.get('halal_sto', '') == 'on', halal_kt=request.POST.get('halal_kt', '') == 'on', no_alc_d=request.POST.get('no_alc_d', '') == 'on', no_alc_m=request.POST.get('no_alc_m', '') == 'on', posted_at=timezone.now())
    # restaurant.save()


def top(request):
    restaurants = Restaurant.objects.filter(
        ratings__isnull=False).order_by('-ratings__average')[:3]

    if request.method == 'POST':
        form = FindForm(request.POST or None)
        if form.is_valid():
            request.session['find'] = form.cleaned_data
            return redirect('list')
    else:
        context = {
            'form': FindForm(),
            'restaurants': restaurants,
        }
        return render(request, 'HalalWeb/top.html', context)


def list(request):

    list = request.session.get('find')

    restaurants = Restaurant.objects.filter(
        genre=list['genre'],
        halal_cer=list['halal_cer'],
        halal_meat=list['halal_meat'],
        p_free=list['p_free'],
        halal_sea=list['halal_sea'],
        halal_meal=list['halal_meal'],
        vege_meal=list['vege_meal'],
        muslim_c=list['muslim_c'],
        muslim_o=list['muslim_o'],
        halal_sto=list['halal_sto'],
        halal_kt=list['halal_kt'],
        no_alc_d=list['no_alc_d'],
        no_alc_m=list['no_alc_m'],

    )
    context = {'restaurants': restaurants}

    return render(request, 'HalalWeb/list.html', context)


def restaurant(request, restaurant_id):
    try:
        restaurant = Restaurant.objects.get(pk=restaurant_id)
    except Restaurant.DoesNotExist:
        raise Http404("Restaurant does not exist")

    if request.method == 'POST':
        if not request.user.is_authenticated:
            return redirect('login')
        comment = Comment(restaurant=restaurant,
                          detail=request.POST['detail'],
                          user_id=request.user.id,
                          created=timezone.now())
        comment.save()

    comments = restaurant.comments.order_by('created')

    context = {
        'restaurant': restaurant,
        'comments': comments,
    }

    return render(request, 'HalalWeb/restaurant.html', context)


def register(response):
    if response.method == "POST":
        form = RegisterForm(response.POST)
        if form.is_valid():
            form.save()

            return redirect("top")
    else:
        form = RegisterForm()

    return render(response, "HalalWeb/register.html", {"form": form})

def about(request):
    return render(request, 'HalalWeb/about.html')
