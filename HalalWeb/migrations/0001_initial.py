# Generated by Django 2.2.5 on 2020-01-15 21:21

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Restaurant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('gen_info', models.TextField()),
                ('genre', models.CharField(choices=[('JP', 'Japanese'), ('CH', 'Chinese'), ('WE', 'Western'), ('TH', 'Thai'), ('IN', 'Indian'), ('OT', 'Other')], default='OT', max_length=2)),
                ('menu', models.TextField()),
                ('halal_cer', models.BooleanField(default=False)),
                ('halal_meat', models.BooleanField(default=False)),
                ('p_free', models.BooleanField(default=False)),
                ('halal_sea', models.BooleanField(default=False)),
                ('halal_meal', models.BooleanField(default=False)),
                ('vege_meal', models.BooleanField(default=False)),
                ('muslim_o', models.BooleanField(default=False)),
                ('muslim_c', models.BooleanField(default=False)),
                ('halal_sto', models.BooleanField(default=False)),
                ('halal_kt', models.BooleanField(default=False)),
                ('no_alc_d', models.BooleanField(default=False)),
                ('no_alc_m', models.BooleanField(default=False)),
                ('posted_at', models.DateTimeField(auto_now=True)),
                ('image', models.CharField(max_length=100)),
                ('map_lat', models.CharField(max_length=100)),
                ('map_long', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField()),
                ('detail', models.TextField()),
                ('restaurant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comments', to='HalalWeb.Restaurant')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comments', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
