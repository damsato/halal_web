from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from star_ratings.models import Rating
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User

# Create your models here.


class Restaurant(models.Model):
    name = models.CharField(max_length=100)  # restaurant name
    gen_info = models.TextField()  # restaurant general info
    JAPANESE = 'JP'
    CHINESE = 'CH'
    WESTERN = 'WE'
    THAI = 'TH'
    INDIAN = 'IN'
    OTHER = 'OT'
    GENRE_CHOICES = [
        (JAPANESE, 'Japanese'),
        (CHINESE, 'Chinese'),
        (WESTERN, 'Western'),
        (THAI, 'Thai'),
        (INDIAN, 'Indian'),
        (OTHER, 'Other'),
    ]
    genre = models.CharField(
        max_length=2,
        choices=GENRE_CHOICES,
        default=OTHER,
    )  # genre
    menu = models.TextField()  # restaurant menu
    halal_cer = models.BooleanField(default=False)  # Halal Certified
    halal_meat = models.BooleanField(default=False)  # Halal Meat
    p_free = models.BooleanField(default=False)  # Pork Free
    halal_sea = models.BooleanField(default=False)  # Halal Seasoning
    halal_meal = models.BooleanField(default=False)  # Halal Meal
    vege_meal = models.BooleanField(default=False)  # Vegetarian Meal
    muslim_o = models.BooleanField(default=False)  # Muslim Owner
    muslim_c = models.BooleanField(default=False)  # Muslim Cook
    halal_sto = models.BooleanField(default=False)  # Halal Storage
    halal_kt = models.BooleanField(default=False)  # Halal Kitchen & Tableware
    no_alc_d = models.BooleanField(default=False)  # No Alcoholic Drinks
    no_alc_m = models.BooleanField(default=False)  # No Alcohol in Meal
    posted_at = models.DateTimeField(auto_now=True)  # created
    image = models.CharField(max_length=100)  # link of image
    map_lat = models.CharField(max_length=100)  # coordinate latitude
    map_long = models.CharField(max_length=100)  # coordinate longitude
    ratings = GenericRelation(Rating, related_query_name='restaurant')

    def __str__(self):
        return self.name


#
# class Ratings(models.Model):
#   ratings = GenericRelation(Rating, related_query_name='foos')


class Comment(models.Model):
    created = models.DateTimeField()
    detail = models.TextField()
    restaurant = models.ForeignKey(Restaurant,
                                   related_name='comments',
                                   on_delete=models.CASCADE)
    user = models.ForeignKey(User,
                             related_name='comments',
                             on_delete=models.CASCADE)
