from django import forms
from .models import Restaurant
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe


class RegistrationForm(forms.ModelForm):

    class Meta:
        model = Restaurant
        fields = '__all__'


class FindForm(forms.ModelForm):
    
    class Meta:
        model = Restaurant
        fields = '__all__'
        exclude = ('name', 'menu', 'map_lat', 'map_long', 'gen_info', 'image',)

class RegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ["username", "email", "password1", "password2"]
        
